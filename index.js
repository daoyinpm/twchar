import { getStroke } from "./stroke/index.js";
import { strokeExt, SHIFT_NUM_VAL } from "./trad_extension/index.js";
import { getConvertWordCnZh, getConvertWordListCnZh } from "./convert/index.js";

let CUSTOM_STROKE = null;

const SORTED_MAXNUM = 12;
const MAXNUM_STROKE = 100;
const EMPTY_VALUE = "-";
const REPLACE_VALUE = 99;
const ORDER = { DES:1, ASC:2 };

// 客制化的 stroke
const setCustomStroke = (strokeJson) => {
    if (!strokeJson) {
        return;
    }
    CUSTOM_STROKE = Object.assign({}, strokeJson);
}

// Object hasOwnProperty() changed to Object.prototype.hasOwnProperty.call()
const hasProperty = (obj, property) => Object.prototype.hasOwnProperty.call(obj, property);

// return: [16, 8, 9, 8, 16]
const getStrokeWordByWord = sentence => {
    if (typeof sentence !== 'string') {
        return [];
    }
    return sentence.split('').map(w => w ? getStroke(w) : MAXNUM_STROKE);
};


// 陣列中有文字也有數字時...
// return: [16, 8, 9, 8, 16]
// customStroke: {1: '一', 2: '二'}
const getStrokeWordByWordAdv = (sentence) => {
    if (typeof sentence !== 'string') {
        return [];
    }

    return sentence.split('').map(w => {
        // 如果有使用者定義的 stroke，先處理
        if (CUSTOM_STROKE) {
            const cusStroke = Object.keys(CUSTOM_STROKE).find(stroke => {
                if (CUSTOM_STROKE[stroke].indexOf(w) > -1) {
                    return parseInt(stroke, 10);
                }
                return 0;
            });

            if (cusStroke) {
                return cusStroke;
            }
        }

        // 先從自行定義的取值，若有值則回傳
        const ext = strokeExt(w);
        if (ext) {
            return parseInt(ext, 10);
        }

        // 由大資料庫取 stroke
        const stroke = getStroke(w);

        // 避免與數字重複，中文筆劃從20開始
        return stroke ? stroke + SHIFT_NUM_VAL : MAXNUM_STROKE;
    });
};



// results: [{...}...]
// {...}:
// description: "台灣文學發展..."
// id: "CARDlh"
// imgUrl: "https://nmtl-fs.daoyidh.com/static/lh/lh-vr-icon.jpeg"
// order: "1"
// title: "賴和紀念館"
// type: "vr"
function sortedByStroke (results, key) {
    if (!results) {
        return [];
    }

    // 只做一次 nameStroke 節省時間
    const nameStroke = {};
    for (let idx = 0; idx < results.length; idx += 1) {
        const name = hasProperty(results[idx], key) ? results[idx][key] : '';
        // 取出前五個字的陣列
        nameStroke[name] = getStrokeWordByWord(name.slice(0, SORTED_MAXNUM));
    }

    return results.sort((a, b) => {
        if (!hasProperty(a, key) || !hasProperty(b, key)) {
            return 0;
        }

        // 取出前五個字的陣列
        const valA = nameStroke[a[key]];
        const valB = nameStroke[b[key]];

        for (let idx = 0; idx < SORTED_MAXNUM; idx += 1) {
            // 0 代表此字無筆劃，需擴充。放到最後顯示
            if (valA[idx] === 0) {
                // eslint-disable-next-line no-console
                // 擺到最後面
                valA[idx] = MAXNUM_STROKE;
            }
            if (valB[idx] === 0) {
                // eslint-disable-next-line no-console
                // 擺到最後面
                valB[idx] = MAXNUM_STROKE;
            }
            // 如果筆劃相同，繼續比對。
            if (valA[idx] === valB[idx]) {
                if (a[key][idx] === b[key][idx]) {
                    // eslint-disable-next-line no-continue
                    continue;
                } else {
                    return a[key][idx].charCodeAt(0) - b[key][idx].charCodeAt(0);
                }
            }
            return valA[idx] - valB[idx];
        }
        // 如果前幾個字的筆劃都相同，以字的長度來比。
        return a[key].length - b[key].length;
    });
}

// 文字和數字排序
// inputArr: [{...}...]
// {...}: localNameId__string: [{…}],nnBestKnownName__string: [{…}]
// keyArr: ['localNameId__string', 'nnBestKnownName__string']
// order: [ORDER.DES, ORDER.ASC]
// getValFunc: safeGet
// sortLen: 12
function sortedByStrokeAdv ({inputArr, keyArr, getValFunc=null,
                                sortLen=SORTED_MAXNUM, order=[]}) {

    if (!inputArr) {
        return [];
    }
    // 只做一次 nameStroke 節省時間
    const nameStroke = {};
    for (let idx = 0; idx < inputArr.length; idx += 1) {

        const row = inputArr[idx];
        let name='';
        let sortStroke = [];

        if (getValFunc) {
            keyArr.forEach((k, idx)=>{
                // 根據header抓出值，並限制字元長度，若有缺值則補"0"
                const headerSlice = getValFunc(row[k], [0, "label"], EMPTY_VALUE).slice(0, sortLen).padEnd(sortLen,"0");
                name += headerSlice;

                // 筆劃陣列
                let headerSliceStroke = getStrokeWordByWordAdv(headerSlice);

                // 判斷order設定是升冪還是降冪
                const orderEnum = hasProperty(order, idx) ? order[idx] : ORDER.DES;
                if (orderEnum !== ORDER.DES) {
                    // adjust headerSliceStroke, upsidedown the value
                    headerSliceStroke = headerSliceStroke.map(s => {
                        if(s === REPLACE_VALUE){
                            return s;
                        }
                        return -s;
                    });
                }

                // sortStroke = [-2, -10, -10, -1, 28, 29, 22, 25]
                sortStroke = sortStroke.concat(headerSliceStroke);
            })
        } else {
            keyArr.forEach(k=>{
                const val = hasProperty(row, k) ? row[k] : EMPTY_VALUE;
                name += val.slice(0, sortLen).padEnd(sortLen,"0");
            })
            // 筆畫陣列
            sortStroke = getStrokeWordByWordAdv(name);
        }
        // 1990武俠人生:[-2, -10, -10, -1, 28, 29, 22, 25]
        nameStroke[name] = sortStroke;
    }

    return inputArr.sort((a, b) => {
        // valA: [10, 11, 8, 3, 1]
        // valB: [10, 11, 5, 3, 1]
        let valA, valB;

        let nameA = '', nameB = '';
        if (getValFunc) {
            keyArr.forEach(k=>{
                nameA += getValFunc(a[k], [0, "label"], EMPTY_VALUE).slice(0, sortLen).padEnd(sortLen,"0");
            })
            keyArr.forEach(k=>{
                nameB += getValFunc(b[k], [0, "label"], EMPTY_VALUE).slice(0, sortLen).padEnd(sortLen,"0");
            })
        } else {
            keyArr.forEach(k=>{
                const val = hasProperty(a, k) ? a[k] : EMPTY_VALUE;
                nameA += val.slice(0, sortLen).padEnd(sortLen,"0");
            })
            keyArr.forEach(k=>{
                const val = hasProperty(b, k) ? b[k] : EMPTY_VALUE;
                nameB += val.slice(0, sortLen).padEnd(sortLen,"0");
            })
        }

        // valA:: 000033000000
        // valB:: 000033000001
        valA = nameStroke[nameA];
        valB = nameStroke[nameB];


        // Get maxLen
        const maxLen = keyArr.length * sortLen;

        for (let idx = 0; idx < maxLen; idx += 1) {
            // 0 代表此字無筆劃，需擴充。放到最後顯示
            if (valA[idx] === 0) {
                // 擺到最後面
                valA[idx] = MAXNUM_STROKE;
            }
            if (valB[idx] === 0) {
                // 擺到最後面
                valB[idx] = MAXNUM_STROKE;
            }
            // 如果筆劃相同，繼續比對。
            if (valA[idx] === valB[idx]) {
                continue;
            }

            return valA[idx] - valB[idx];
        }

        // 若前面都相同，比較最後一欄的內容長度
        const lastHeader = keyArr.slice(-1);
        let lenA = 0, lenB = 0;

        if (getValFunc) {
            lenA = getValFunc(a[lastHeader], [0, "label"], EMPTY_VALUE).length;
            lenB = getValFunc(b[lastHeader], [0, "label"], EMPTY_VALUE).length;
        } else {
            lenA = hasProperty(a, lastHeader) ? a[lastHeader].length : 0;
            lenB = hasProperty(b, lastHeader) ? b[lastHeader].length : 0;
        }

        return lenA - lenB;
    });
}


const sortedByVRLit = (results, key) => {
    if (!results) {
        return [];
    }
    return results.sort((a, b) => {
        if (!hasProperty(a, key) || !hasProperty(b, key)) {
            return 0;
        }
        return a[key] - b[key];
    });
};

function sortedResutls (resData, key) {
    if (!resData || !key) {
        return [];
    }
    // 筆劃排序
    const strokeSorted = sortedByStroke(resData, key);

    // VR文學館排前面
    return sortedByVRLit(strokeSorted, 'order');
}

function sortedResutlsAdv ({inputArr=[], keyArr=[], getValFunc=null,
                               sortLen=SORTED_MAXNUM, order=ORDER.DES}) {
    if (!inputArr || inputArr.length === 0 || !keyArr || keyArr.length === 0) {
        return [];
    }
    // 筆劃排序
    const strokeSorted = sortedByStrokeAdv ({inputArr, keyArr, getValFunc, sortLen, order});

    // VR文學館排前面
    return sortedByVRLit(strokeSorted, 'order');
}

function sortedAdvancedSearch (resData, key) {
    if (!resData || !key) {
        return [];
    }
    // 筆劃排序
    const strokeSorted = sortedByStroke(resData, key);

    // 種類排序
    const orderType = ['person', 'publication', 'article', 'organization', 'event'];
    return strokeSorted.sort((a, b) => {
        const idxA = orderType.indexOf(a.type);
        const idxB = orderType.indexOf(b.type);
        return idxA - idxB;
    });
}

/**
 * 取得繁簡字表(string), used for API
 * @param word
 * @returns {string}
 *
 * e.g.
 * "國家" => "[國国][家]"
 * "請保證最先引入 cnchar 基础库"=> "[請请][保][證证][最][先][引][入][ ][c][n][c][h][a][r][ ][基][礎础][庫库]"
 */
const getTradSimpleWordStr = (word) => getConvertWordCnZh(word);

/**
 * 取得繁簡字表(array)
 * @param word
 * @returns {string[]}
 *
 * e.g.
 * "國家" => [["國","国"],["家"]]
 */
const getTradSimpleWordList = (word) => getConvertWordListCnZh(word);


/**
 * Module exports.
 * @public
 */


export {
    ORDER,
    setCustomStroke,
    sortedAdvancedSearch,
    sortedResutls,
    sortedResutlsAdv,
    sortedByStroke,
    sortedByStrokeAdv,
    getTradSimpleWordStr,
    getTradSimpleWordList,
    getStrokeWordByWordAdv
}
