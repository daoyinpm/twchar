# twchar

sorting Chinese words

# Usage

### 排序 
```
import twchar from 'twchar';
const { sortedResutls } = twchar;

const array = [
    {'a': 'aaaa', 'key': '九天十'},
    {'a': 'bbbb', 'key': '兩山高'},
    {'a': 'cccc', 'key': '一山大'},
    {'a': 'dddd', 'key': '八九十'},
    {'a': 'eeee', 'key': '龜難山'},
    {'a': 'ffff', 'key': '六七八'},
    {'a': 'ggggg', 'key': '一二三'}
]
const res = sortedResutls(array, 'key');
console.log(res);
```

### 繁簡轉換
```js
import twchar from 'twchar';
const { getTradSimpleWordStr, getTradSimpleWordList } = twchar;

const text = "國家";
const res1 = getTradSimpleWordStr(text);
console.log(res1);
// "[國国][家]"

const res2 = getTradSimpleWordList(text);
console.log(res2)
// [["國","国"],["家"]]


```

# export functions
```
import twchar from 'twchar';
const { 
    sortedAdvancedSearch, 
    sortedResutls, 
    sortedByStroke, 
    getTradSimpleWordStr,
    getTradSimpleWordList
} = twchar;
```
