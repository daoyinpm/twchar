const SHIFT_NUM_VAL = 20;

const charStroke = {
    // 數字筆劃
    '1': '0',
    '2': '1',
    '3': '2',
    '4': '3',
    '5': '4',
    '6': '5',
    '7': '6',
    '8': '7',
    '9': '8',
    '10': '9',
    // 中文字筆劃
    // SHIFT_NUM_VAL
    '21': '一',
    '22': '二',
    '23': '三',
    '24': '四',
    '25': '五',
    '26': '六',
    '27': '七',
    '28': '八',
    '29': '九',
    '30': '十',
    // 英文筆劃從 50 開始
    '50': 'aA',
    '51': 'bB',
    '52': 'cC',
    '53': 'dD',
    '54': 'eE',
    '55': 'fF',
    '56': 'gG',
    '57': 'hH',
    '58': 'iI',
    '59': 'jJ',
    '60': 'kK',
    '61': 'lL',
    '62': 'mM',
    '63': 'nN',
    '64': 'oO',
    '65': 'pP',
    '66': 'qQ',
    '67': 'rR',
    '68': 'sS',
    '69': 'tT',
    '70': 'uU',
    '71': 'vV',
    '72': 'wW',
    '73': 'xX',
    '74': 'yY',
    '75': 'zZ',
    '99': '-',
}

const strokeExt = (char) => {
    return Object.keys(charStroke).find(stroke => {
        if (charStroke[stroke].indexOf(char) > -1) {
            return parseInt(stroke, 10);
        }
        return 0;
    });
}

export {
    strokeExt,
    SHIFT_NUM_VAL,
}
