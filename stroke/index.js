const DefaultStroke = 99;

const cnStrokeList = {
    0: '',
    1: '一乙',
    2: '丁七乃乜九了二人亻儿入八冂几凵刀刁力勹匕十厂厶又卜乂',
    3: '万丈三上下丌个丫丸久乇么义乞也习乡亍于亏亡亿兀凡刃勺千卫叉口囗土士夕大女子孑孓寸小尢尸山巛川工己已巳巾干幺广廾弋弓才门飞马与之及',
    4: '不丐丑专中丰丹为乌书予云互亓五井亢什仁仂仃仄仅仆仇仉今介仍从仑仓允元公六兮内冈冗凤凶分切刈劝办勾勿匀化匹区卅升午卞厄厅历友双反壬天太夫夭孔少尤尹尺屯巴币幻廿开引心忆戈户手扎支攴攵文斗斤方无日曰月木欠止歹殳毋比毛氏气水火爪父爻爿片牙牛犬王瓦肀艺见计订讣认讥贝车邓长闩队韦风乏以巨巿冇',
    5: '讬且丕世丘丙业丛东丝主乍乎乐仔仕他仗付仙仝仞仟仡代令仨仪仫们兄兰冉册写冬冯凸凹出击刊刍功加务劢包匆北匝卉半卟占卡卢卮卯厉去发古句另叨叩只叫召叭叮可台叱史右叵叶号司叹叻叼叽囚四圣处外央夯失头奴奶孕宁它宄对尔尕尻尼左巧市布帅平幼庀弁弗弘归必忉戊戋扑扒打扔斥旦旧未末本札术正母氐民氕永汀汁汇汉灭犯犰玄玉瓜甘生用甩田由甲申电疋白皮皿目矛矢石示礼禾穴立纠艽艾艿节讦讧讨让讪讫训议讯记轧边辽邗邙邛邝钅闪阡阢饥驭鸟龙印戉氹奵',
    6: '丞丢乒乓乔乩买争亘亚交亥亦产仰仲仳仵件价任份仿企伉伊伍伎伏伐休众优伙会伛伞伟传伢伤伥伦伧伪伫佤充兆先光全共关兴再军农冰冱冲决凫凼刎刑划刖列刘则刚创劣动匈匠匡华协危压厌厍吁吃各吆合吉吊同名后吏吐向吒吓吕吖吗囝回囟因囡团在圩圪圬圭圮圯地圳圹场圾壮夙多夷夸夹夺夼奸她好妁如妃妄妆妇妈字存孙宅宇守安寺寻导尖尘尥尧尽屹屺屿岁岂岌州巡巩帆师年并庄庆延廷异式弛当忏忖忙戌戍戎戏成托扛扣扦执扩扪扫扬收旨早旬旭旮旯曲曳有朱朴朵机朽杀杂权次欢此死毕氖氘氽汆汊汐汔汕汗汛汜汝江池污汤汲灯灰爷牝牟犴犷犸玎玑百祁竹米糸纡红纣纤纥约级纨纩纪纫缶网羊羽老考而耒耳聿肉肋肌臣自至臼舌舛舟艮色芄芊芋芍芎芏芑芒芗芝芨虍虫血行衣西观讲讳讴讵讶讷许讹论讼讽设访诀贞负轨达迁迂迄迅过迈邡邢那邦邪邬钆钇闫闭问闯阪阮阱防阳阴阵阶页饧驮驯驰齐似吸芃邨吋贠孖屾叒玏岀甪',
    7: '两严串丽乱亨亩伯估伲伴伶伸伺伽佃但位低住佐佑体何佗佘余佚佛作佝佞佟你佣佥佧克免兑兕兵况冶冷冻初删判刨利别刭助努劫劬劭励劲劳匣医卣卤即却卵县君吝吞吟吠吡吣否吧吨吩含听吭吮启吱吲吴吵吹吻吼吾呀呃呆呈告呋呐呒呓呔呕呖呗员呙呛呜囤囫园困囱围囵圻址坂均坊坌坍坎坏坐坑块坚坛坜坝坞坟坠声壳奁奂妊妍妒妓妖妗妙妞妣妤妥妨妩妪妫姊姒孚孛孜孝宋完宏寿尬尾尿局屁层岈岍岐岑岔岖岗岘岙岚岛岜希帏帐庇床庋序庐庑库应弃弄弟张形彤彷役彻忌忍忐忑忒志忘忡忤忧忪快忭忮忱忸忻忾怀怃怄怅怆我戒扭扮扯扰扳扶批扼找技抄抉把抑抒抓投抖抗折抚抛抟抠抡抢护报拒拟攸改攻旰旱时旷更杆杈杉杌李杏材村杓杖杜杞束杠条来杨杩极欤步歼每氙氚求汞汨汩汪汰汴汶汹汽汾沁沂沃沅沆沈沉沌沏沐沔沙沛沟没沣沤沥沦沧沩沪泐泛灵灶灸灼灾灿炀牡牢状犹狁狂狃狄狈玖玛甫甬男甸町疔疖疗皂盯矣矶社祀秀私秃究穷系纬纭纯纰纱纲纳纵纶纷纸纹纺纽纾罕羌肓肖肘肚肛肜肝肟肠良芈芘芙芜芟芡芤芥芦芩芪芫芬芭芮芯芰花芳芴芷芸芹芽芾苁苄苇苈苊苋苌苍苎苏苡苣虬补角言证诂诃评诅识诈诉诊诋诌词诎诏译诒谷豆豕豸贡财赤走足身轩轫辛辰迎运近迓返迕还这进远违连迟邑邮邯邰邱邳邴邵邶邸邹邺邻酉里针钉钊钋钌闰闱闲闳间闵闶闷阻阼阽阿陀陂附际陆陇陈陉韧饨饩饪饫饬饭饮驱驳驴鸠鸡麦龟巫囧旸佔飏呎釆佈玙囯沕',
    8: '丧乖乳事些亟享京佩佬佯佰佳佴佶佻佼佾使侃侄侈侉例侍侏侑侔侗供依侠侣侥侦侧侨侩侪侬兔兖其具典冼冽净凭凯函刮到刳制刷券刹刺刻刽刿剀剁剂劾势匦卑卒卓单卖卦卧卷卺厕叁参叔取呢呤呦周呱味呵呶呷呸呻呼命咀咂咄咆咋和咎咏咐咒咔咕咖咙咚咛咝哎囹固国图坡坤坦坨坩坪坫坭坯坳坶坷坻坼垂垃垄垅垆备夜奄奇奈奉奋奔妮妯妲妹妻妾姆始姐姑姓委姗孟孢季孤孥学宓宕宗官宙定宛宜宝实宠审尚居屈屉届岢岣岩岫岬岭岱岳岵岷岸岽岿峁峄帑帔帕帖帘帙帚帛帜幸底庖店庙庚府庞废建弥弦弧弩弪录彼往征徂径忝忠念忽忿态怂怊怍怏怔怕怖怙怛怜怡怦性怩怪怫怯怵怿戕或戗戽戾房所承抨披抬抱抵抹抻押抽抿拂拄担拆拇拈拉拊拌拍拎拐拓拔拖拗拘拙拚招拢拣拥拦拧拨择放斧斩於旺昀昂昃昆昊昌明昏易昔昕昙朊朋服杪杭杯杰杲杳杵杷杼松板构枇枉枋析枕林枘枚果枝枞枢枣枥枧枨枪枫枭柜欣欧武歧殁殴氓氛沓沫沭沮沱沲河沸油治沼沽沾沿泄泅泊泌泓泔法泖泗泞泠泡波泣泥注泪泫泮泯泱泳泷泸泺泻泼泽泾浅炅炉炊炎炒炔炕炖炙炜炝炬爬爸版牦牧物狍狎狐狒狗狙狞玟玢玩玫玮环现瓮瓯甙画甾畀畅疙疚疝疟疠疡的盂盱盲直知矸矽矾矿砀码祆祈祉秆秉穸穹空竺籴线绀绁绂练组绅细织终绉绊绋绌绍绎经绐罔罗者耵耶肃股肢肤肥肩肪肫肭肮肯肱育肴肷肺肼肽肾肿胀胁臾舍艰苑苒苓苔苕苗苘苛苜苞苟苠苤若苦苫苯英苴苷苹苻茁茂范茄茅茆茇茉茌茎茏茑茔茕茚虎虏虮虱表衩衫衬规觅视诓诔试诖诗诘诙诚诛诜话诞诟诠诡询诣诤该详诧诨诩责贤败账货质贩贪贫贬购贮贯转轭轮软轰迢迤迥迦迨迩迪迫迭迮述迳邾郁郄郅郇郊郎郏郐郑郓采金钍钎钏钐钒钓钔钕钗闸闹阜陋陌降限陔陕隶隹雨青非顶顷饯饰饱饲饴驵驶驷驸驹驺驻驼驽驾驿骀鱼鸢鸣黾齿受变弢祎玥旻昇劼昉咘旼劵毑祇叕炘泃佺',
    9: '临举亭亮亲侮侯侵便促俄俅俊俎俏俐俑俗俘俚俜保俞俟信俣俦俨俩俪俭修兹养冒冠剃削剌前剐剑勃勇勉勋匍南卸厘厚叙叛呲咣咤咦咧咨咩咪咫咬咭咯咱咳咴咸咻咽咿哀品哂哄哆哇哈哉哌响哏哐哑哒哓哔哕哗哙哚哜哝哞哟哪囿型垌垒垓垛垠垡垢垣垤垦垧垩垫垭垮垲垴城埏复奎奏契奕奖姘姚姜姝姣姥姨姹姻姿威娃娄娅娆娇娈娜孩孪客宣室宥宦宪宫封将尜尝屋屎屏峋峒峙峡峤峥峦差巷帝带帧帮幽庠庥度庭弈弭弯彖彦待徇很徉徊律後怎怒思怠急怨总怼恂恃恍恒恢恤恨恪恫恬恸恹恺恻恼恽战扁扃拜括拭拮拯拱拴拶拷拼拽拾持挂指按挎挑挖挝挞挟挠挡挢挣挤挥挪挺政故斫施既昝星映春昧昨昭是昱昴昵昶昼显曷朐枯枰枳枵架枷枸柁柃柄柏某柑柒染柔柘柙柚柝柞柠柢查柩柬柯柰柱柳柽柿栀栅标栈栉栊栋栌栎栏树歪殂殃殄殆殇残段毒毖毗毡氟氡氢泉泵泶洁洄洇洋洌洎洒洗洙洚洛洞津洧洪洫洮洱洲洳洵洹活洼洽派浃浇浈浊测浍济浏浑浒浓浔涎炫炭炮炯炱炳炷炸点炻炼炽烀烁烂烃爰牮牯牲牵狠狡狨狩独狭狮狯狰狱狲玲玳玷玻珀珂珈珉珊珍珏珐珑瓴甚甭畈畋界畎畏疣疤疥疫疬疮疯癸皆皇皈盅盆盈相盹盼盾省眄眇眈眉看眍眨矜矧矩砂砉砌砍砑砒研砖砗砘砚砜砭祓祖祗祚祛祜祝神祠祢禹禺秋种科秒秕秭穿窀突窃窆竖竽竿笃笈类籼籽绑绒结绔绕绗绘给绚绛络绝绞统缸罘罚美羿耍耐耔耷胂胃胄胆背胍胎胖胗胙胚胛胜胝胞胡胤胥胧胨胩胪胫脉舁舡舢舣茈茗茛茜茧茨茫茬茭茯茱茳茴茵茶茸茹茺茼荀荃荆荇草荏荐荑荒荔荚荛荜荞荟荠荡荣荤荥荦荧荨荩荪荫荬荭荮药莒莛虐虹虺虻虼虽虾虿蚀蚁蚂蚤衍衲衽衿袂袄要觇览觉訇诫诬语诮误诰诱诲诳说诵诶贰贱贲贳贴贵贶贷贸费贺贻赳赴赵趴轱轲轳轴轵轶轷轸轹轺轻迷迸迹追退送适逃逄逅逆选逊郗郛郜郝郡郢郦郧酊酋重钙钚钛钜钝钞钟钠钡钢钣钤钥钦钧钨钩钪钫钬钭钮钯闺闻闼闽闾阀阁阂陛陟陡院除陧陨险面革韭音顸项顺须飑飒食饵饶饷饺饼首香骁骂骄骅骆骇骈骨鬼鸥鸦鸨鸩叟恰祐昺垚珅玹洸屌浕浐畑闿咲怹昳陞査洺栃垵炤姮垟恆饹竑剎饸垕',
    10: '袅乘亳俯俱俳俸俺俾倌倍倏倒倔倘候倚倜借倡倥倦倨倩倪倬倭倮债值倾偌健党兼冢冤冥凄准凇凉凋凌剔剖剜剞剡剥剧勐匪匿卿厝原哥哦哧哨哩哭哮哲哳哺哼哽哿唁唆唇唉唏唐唑唔唛唠唢唣唤唧啊圃圄圆垸埂埃埋埒埔埕埘埙埚壶夏套奘奚姬娉娌娑娓娘娟娠娣娥娩娱娲娴婀孬宰害宴宵家宸容宽宾射屐屑展屙峨峪峭峰峻崂崃席帱座弱徐徒徕恁恋恐恕恙恚恝恣恧恩恭息恳恶悃悄悌悍悒悔悖悚悛悝悟悦悭悯扇拳拿挈挚挛挨挫振挹挽捂捃捅捆捉捋捌捍捎捏捐捕捞损捡换捣效敉敌敖斋料旁旃旄旅旆晁晃晋晌晏晒晓晔晕晖晟朔朕朗柴栓栖栗栝校栩株栲栳样核根格栽栾桀桁桂桃桄桅框案桉桊桌桎桐桑桓桔桕桠桡桢档桤桥桦桧桨桩梃梆殉殊殷毙毪氤氦氧氨氩泰流浆浙浚浜浞浠浣浦浩浪浮浯浴海浸浼涂涅消涉涌涑涓涔涕涛涝涞涟涠涡涣涤润涧涨涩烈烊烘烙烛烟烤烦烧烨烩烫烬热爱爹特牺狳狴狷狸狺狻狼猁猃玺珙珞珠珥珧珩班珲琊瓞瓶瓷畔留畚畛畜疰疱疲疳疴疸疹疼疽疾痂痃痄病症痈痉皋皱益盍盎盏盐监眙眚真眠眢眩砝砟砣砥砧砩砬砰破砷砸砹砺砻砼砾础祟祥祧祯离秘租秣秤秦秧秩秫积称窄窈窍站竞笄笆笊笋笏笑笔笕笫粉粑紊素索紧绠绡绢绣绥绦继绨缺罟罡罢羔羞翁翅耄耆耕耖耗耘耙耸耻耽耿聂胭胯胰胱胲胳胴胶胸胺胼能脂脆脊脍脎脏脐脑脒脓臬臭致舀舐舨航舫般舭舯舰舱艳荷荸荻荼荽莅莆莉莎莓莘莜莞莠莨莩莪莫莰莱莲莳莴莶获莸莹莺莼莽虑虔蚊蚋蚌蚍蚓蚕蚜蚝蚣蚧蚨蚩蚪蚬衄衮衰衷衾袁袍袒袖袜袢被觊请诸诹诺读诼诽课诿谀谁谂调谄谅谆谇谈谊豇豹豺贼贽贾贿赀赁赂赃资赅赆赶起趵趸趿躬軎轼载轾轿辁辂较辱逋逍透逐逑递途逖逗通逛逝逞速造逡逢逦邕部郫郭郯郴郸都酌配酎酏酐酒釜钰钱钲钳钴钵钶钷钸钹钺钻钼钽钾钿铀铁铂铃铄铅铆铈铉铊铋铌铍铎阃阄阅阆陪陬陲陴陵陶陷隼隽难顼顽顾顿颀颁颂颃预饽饿馀馁骊骋验骏高髟鬯鬲鸪鸫鸬鸭鸯鸱鸲鸳鸵鸶龀彧翀珮埇崀俶疍翃珣峯埗埈倓珪娭鸮栢烜託珽脩挼',
    11: '彪晚梢梧梨龛乾偃假偈偎偏偕做停偬偶偷偻偾偿傀兜兽冕减凑凰剪副勒勖勘匏匐匙匮匾厢厣厩唪唬售唯唰唱唳唷唼唾唿啁啃啄商啉啐啕啖啜啡啤啥啦啧啪啬啭啮啵啶啷啸喏喵圈圉圊埝域埠埤埭埯埴埸培基埽堀堂堆堇堋堍堑堕堵够奢娶娼婆婉婊婕婚婢婧婪婴婵婶孰宿寂寄寅密寇尉屠崆崇崎崔崖崛崞崤崦崧崩崭崮巢帷常帻帼庳庵庶康庸庹庾廊弹彗彩彬得徘徙徜恿悉悠患您悫悬悱悴悸悻悼情惆惊惋惕惘惚惜惝惟惦惧惨惬惭惮惯戚戛扈挲捧捩捭据捱捶捷捺捻掀掂掇授掉掊掎掏掐排掖掘掠探接控推掩措掬掭掮掳掴掷掸掺掼揶敏救敕教敛敝敢斛斜断旋旌旎族晗晡晤晦晨曹曼望桫桴桶桷梁梅梏梓梗梦梭梯械梳梵检棂欲欷殍殒殓毫氪涪涫涮涯液涵涸涿淀淄淅淆淇淋淌淑淖淘淙淝淞淠淡淤淦淫淬淮深淳混淹添清渊渌渍渎渐渑渔渖渗渚渠烯烷烹烽焉焊焐焓焕焖焘爽牾牿犁猊猎猓猕猖猗猛猜猝猞猡猪猫率球琅理琉琏琐瓠甜略畦疵痊痍痒痔痕痖皎皑皲盒盔盖盗盘盛眦眭眯眵眶眷眸眺眼着睁矫砦硅硇硌硎硐硒硕硖硗硭票祭祷祸秸移秽稆窑窒窕竟章笙笛笞笠笤笥符笨笪第笮笱笳笸笺笼笾筇粒粕粗粘粜粝累绩绪绫续绮绯绰绱绲绳维绵绶绷绸绺绻综绽绾绿缀缁缍羚羝羟翊翌翎耜聃聆聊聋职聍胬脖脘脚脞脬脯脱脲脶脸舂舳舴舵舶舷舸船舻艴菀菁菅菇菊菌菏菔菖菘菜菝菟菠菡菥菩菪菰菱菲菸菹菽萁萃萄萆萋萌萍萎萏萑萘萜萝萤营萦萧萨萸著虚蚯蚰蚱蚴蚵蚶蚺蛀蛄蛆蛇蛉蛊蛋蛎蛏衅衔袈袋袤袭袱袷袼裆裉觋觖谋谌谍谎谏谐谑谒谓谔谕谖谗谘谙谚谛谜谝豉豚象赇赈赉赊赦赧趺趼趾跃跄距躯辄辅辆逭逮逯逵逶逸逻郾鄂鄄酗酚酝酞野铐铑铒铕铖铗铘铙铛铜铝铞铟铠铡铢铣铤铥铧铨铩铪铫铬铭铮铯铰铱铲铳铴铵银铷阈阉阊阋阌阍阎阏阐隅隆隈隋隍随隐隗雀雩雪颅领颇颈馄馅馆馗骐骑骒骓骖鸷鸸鸹鸺鸽鸾鸿鹿麸麻黄龚描珺堃產琍崑啰晞谞硚琇焗埼菂採馃偲琎啟啲堌捯珵惇棻堉',
    12: '亵傅傈傍傣傥傧储傩傲凿剩割募博厥厦厨啻啼啾喀喁喂喃善喇喈喉喊喋喑喔喘喙喜喝喟喧喱喳喷喹喻喽喾嗖嗟堙堞堠堡堤堪堰塄塔壹奠奥婷婺婿媒媚媛媪嫂孱孳富寐寒寓尊就属屡崴崽崾嵇嵋嵌嵘嵛嵝嵫嵬嵯巯巽帽幂幄幅弑强弼彘彭御徨循悲惑惠惩惫惰惴惶惹惺愀愉愎愕愠愣愤愦愧慌慨戟戢扉掌掣掰掾揄揆揉揍揎提插揖揞揠握揣揩揪揭揲援揸揽揿搀搁搂搅搓搔搜搭搽摒敞散敦敬斌斐斑斯普景晰晴晶晷智晾暂暑曾替最朝期棉棋棍棒棕棘棚棠棣森棰棱棵棹棺棼椁椅椋植椎椐椒椟椠椤椭椰楗楮榔欹欺款殖殚殛毯毳毵毽氮氯氰淼渝渡渣渤渥温渫渭港渲渴游渺湃湄湍湎湓湔湖湘湛湟湫湮湾湿溃溅溆溉溲滁滋滑滞焙焚焦焯焰焱然煮牌牍犀犄犊犋犍猢猥猩猬猱猴猸猹猾琚琛琢琥琦琨琪琬琮琰琳琴琵琶琼瑛瓿甥甯番畲畴疏痘痛痞痢痣痤痦痧痨痪痫登皓皖皴睃睇睐睑矬短硝硪硫硬确硷祺禄禅禽稀稂稃程稍税窖窗窘窜窝竣童竦筅等筋筌筏筐筑筒答策筘筚筛筝筵粞粟粢粤粥粪紫絮絷缂缃缄缅缆缇缈缉缋缌缎缏缑缒缓缔缕编缗缘羡翔翕翘耋耠聒联脔脾腆腈腊腋腌腑腓腔腕腙腚腱腴舄舒舜舾艇萱萼落葆葑葙葚葛葜葡董葩葫葬葭葱葳葵葶葸葺蒂蒇蒈蒉蒋蒌蒎蛐蛑蛔蛘蛙蛛蛞蛟蛤蛩蛭蛮蛰蛱蛲蛳蛴蜒蜓街裁裂装裎裒裕裙裢裣裤裥覃觌觚觞詈谟谠谡谢谣谤谥谦谧貂赋赌赍赎赏赐赓赔赕趁趄超越趋跆跋跌跎跏跑跖跗跚跛跞践辇辈辉辊辋辍辎辜逼逾遁遂遄遇遍遏遐遑遒道遗酡酢酣酤酥釉释量铸铹铺铼铽链铿销锁锂锃锄锅锆锇锈锉锊锋锌锍锎锏锐锑锒锓锔锕阑阒阔阕隔隘隙雁雄雅集雇雯雳靓韩颉颊颌颍颏飓飧飨馇馈馊馋骗骘骚骛鱿鲁鲂鹁鹂鹃鹄鹅鹆鹇鹈黍黑黹鼋鼎嗒皙喆犇湉焜圏甦復萩畬琹棪鹀嗞椪翚註堺欻',
    13: '戡缙催傺傻像剽剿勤叠嗄嗅嗉嗌嗍嗑嗓嗔嗜嗝嗡嗣嗤嗥嗦嗨嗪嗫嗬嗯嗲嗳嗵嗷嘟塌塍塑塘塞塥填塬墓媲媳媵媸媾嫁嫉嫌嫒嫔嫫寝寞尴嵊嵩嵴幌幕廉廒廓彀徭微想愁愆愈愍意愚感愫慈慊慎慑戤戥搋搌搏搐搛搞搠搡搦搪搬携摁摄摅摆摇摈摊摸敫数斟新旒暄暇暌暖暗椴椹椽椿楂楔楚楝楞楠楣楦楫楱楷楸楹楼榀概榄榆榇榈榉榘槌槎槐歃歆歇殿毁毂毹氲溏源溘溜溟溢溥溧溪溯溱溴溶溷溺溻溽滂滇滏滓滔滗滚滟滠满滢滤滥滦滨滩漓漠漭煅煊煌煎煜煞煤煦照煨煲煳煸煺牒犏献猷猿瑁瑕瑗瑙瑚瑜瑞瑟瑰甄畸畹痰痱痴痹痼痿瘀瘁瘃瘅瘐盟睚睛睡睢督睥睦睨睫睬睹瞄矮硼碇碉碌碍碎碑碓碗碘碚碛碜碰禀禁禊福稔稗稚稞稠稣窟窠窥窦筠筢筮筱筲筷筹筻签简粮粱粲粳缚缛缜缝缟缠缡缢缣缤罨罩罪置署群羧耢聘肄肆腠腥腧腩腭腮腰腹腺腻腼腽腾腿舅艄艉蒗蒙蒜蒡蒯蒲蒴蒸蒹蒺蒽蒿蓁蓄蓉蓊蓍蓐蓑蓓蓖蓝蓟蓠蓣蓥蓦蓬虞蛸蛹蛾蜂蜃蜇蜈蜉蜊蜍蜕蜗蜣衙裔裘裟裨裰裱裸裼裾褂褚觎觜解觥触訾詹誉誊谨谩谪谫谬豢貅貉貊赖趑趔跟跣跤跨跪跫跬路跳跷跸跹跺跻躲辏辐辑输辔辞辟遘遛遢遣遥遨鄙鄞鄢鄣酩酪酬酮酯酰酱鉴锖锗锘错锚锛锝锞锟锡锢锣锤锥锦锨锩锪锫锬锭键锯锰锱阖阗阙障雉雍雎雏零雷雹雾靖靳靴靶韪韫韵颐频颓颔颖飕馍馏馐骜骝骞骟骰骱髡魁魂鲅鲆鲇鲈鲋鲍鲎鲐鹉鹊鹋鹌鹎鹏鹑麂鼓鼠龃龄龅龆蜀滘勠瑄嬅漷碁榅窣塱塚鄠瑀榊蓢暐跶',
    14: '歌獒僖僚僦僧僬僭僮僳儆兢凳劁劂厮嗽嗾嘀嘁嘈嘉嘌嘎嘏嘘嘛嘞嘣嘤嘧塾墁境墅墉墒墙墚夤夥嫖嫘嫜嫠嫡嫣嫦嫩嫱孵察寡寤寥寨屣嶂幔幛廑廖弊彰愿慕慝慢慵慷截戬搴搿摔摘摞摧摭摹摺撂撄撇撖敲斡旖旗暝暧暨榍榕榛榜榧榨榫榭榱榴榷榻槁槊槔槛槟槠模歉殡毓滴滹漂漆漉漏演漕漤漩漪漫漯漱漳漶漾潆潇潋潍潢潴澉煽熄熊熏熔熘熙熬犒獍獐瑭瑶瑷璃甍疑瘊瘌瘕瘗瘘瘙瘟瘥瘦瘩睽睾睿瞀瞅瞍碟碡碣碥碧碱碲碳碴碹磁磋禚稳窨窬窭竭端箅箍箐箔箕算箜箝管箢箦箧箨箩箪箫箬箸粹粼粽精糁綦綮缥缦缧缨缩缪缫罂罱罴翟翠翡翥耥聚肇腐膀膂膈膊膏膑膜臧舆舔舞艋蓰蓼蓿蔌蔑蔓蔗蔚蔟蔡蔫蔷蔸蔹蔺蔻蔼蔽蕖蜘蜚蜜蜞蜡蜢蜥蜩蜮蜱蜴蜷蜻蜾蜿蝇蝈蝉螂裳裴裹褊褐褓褙褛褡褪觏觫誓谭谮谯谰谱谲豪貌赘赙赚赛赫跽踅踉踊踌辕辖辗辣遭遮鄯鄱酲酴酵酶酷酸酹酽酾酿銎銮锲锴锵锶锷锸锹锺锻锼锾锿镀镁镂镄镅阚隧雌雒需霁霆静靼鞅韬韶颗馑馒骠骡骢骶骷髦魃魄魅鲑鲒鲔鲕鲚鲛鲜鲞鲟鹕鹗鹘鹚鹛鹜麽鼐鼻龇龈墟暮槃頔嫚徳嘚粿漈僰漖製槙樋',
    15: '槭僵僻儇儋凛劈劐勰嘬嘭嘱嘲嘶嘹嘻嘿噌噍噎噔噗噘噙噜噢噶墀增墨墩嬉寮履屦嶙嶝幞幡幢廛影徵德慧慰憋憎憔憧憨憬懂戮摩撅撑撒撕撙撞撤撩撬播撮撰撵撷撸撺擒敷暴暹槲槽槿樊樗樘樟横樯樱橄橡橥毅滕潘潜潦潭潮潲潸潺潼澄澈澌澍澎澜澳熟熠熨熳熵牖獗獠瑾璀璁璇璋璎璜畿瘛瘠瘢瘤瘪瘫瘼瞌瞎瞑瞒瞢碾磅磉磊磐磔磕磙稷稹稻稼稽稿窳箭箱箴篁篆篇篌篑篓糅糇糈糊糌糍缬缭缮缯羯羰翦翩耦耧聩聪膘膛膝膣艏艘蔬蕃蕈蕉蕊蕙蕞蕤蕨蕲蕴蕺虢蝌蝎蝓蝗蝙蝠蝣蝤蝥蝮蝰蝴蝶蝻蝼蝽蝾螋褒褥褫褴觐觑觯谳谴谵豌豫赜赭趟趣踏踔踝踞踟踢踣踩踪踬踮踯踺躺辘遴遵醅醇醉醋醌鋈镆镇镉镊镌镍镎镏镐镑镒镓镔霄震霈霉靠靥鞋鞍鞑鞒题颚颛颜额飘餍馓馔骣骸骺骼髫髯魇鲠鲡鲢鲣鲤鲥鲦鲧鲨鲩鲫鹞鹣鹤麾黎齑龉龊懊镕頫慜樑鋆潟禤噁奭鋐澂',
    16: '儒冀凝劓嘴噤器噩噪噫噬噱噻噼嚆圜墼壁壅嬖嬗嬴寰廨廪徼憝憩憷憾懈懒懔撼擀擂擅操擎擐擗擞整斓暾樨樵樽樾橇橐橘橙橛橱橹橼檎檠歙殪氅氆氇潞澡澧澶澹激濂濉濑濒熹燃燎燔燕燠燧犟獬獭璞瓢甏甑瘭瘰瘳瘴瘵瘸瘾瘿癀癃盥瞟瞠瞥瞰磨磬磲磺禧穆穑窿篙篚篝篡篥篦篪篮篱篷糕糖糗糙缰缱缲缳缴罹羲翮翰翱耨耩耪聱膦膨膪膳臻蕹蕻蕾薄薅薇薏薛薜薤薨薪薮薯螃螅螈融螓螗螟螨螭螯蟆蟒衡褰褶赝赞赠踱踵踹踽蹀蹁蹂蹄蹉辙辚辨辩遽避邀邂鄹醍醐醑醒醚醛錾镖镗镘镙镛镜镝镞镟隰雕霍霎霏霓霖靛鞔鞘颞颟颠颡飙飚餐髭髹髻魈魉鲭鲮鲰鲱鲲鲳鲴鲵鲶鲷鲸鲺鲻鹦鹧鹨鹾麇麈黉黔默鼽璟赟燊嬛濛璠燚磡窸曌曈錤璘蕗嬢叡霑獴',
    17: '儡嚅嚎嚏嚓壑壕嬲嬷孺嶷徽懋懑懦戴擘擢擤擦曙朦檀檄檐檑檗檩檬濞濠濡濮濯燥燮爵獯璐璨璩甓疃癌癍皤瞧瞩瞪瞬瞳瞵磴磷礁礅穗篼篾簇簋簌簏簖簧糜糟糠縻繁繇罄罅罾羁翳翼膺膻臀臁臂臃臆臊臌艚薰薷薹藁藉藏藐藓螫螬螳螵螺螽蟀蟊蟋蟑蟓蟥襁襄觳謇豁豳貔貘赡赢蹇蹈蹊蹋蹑蹒辫邃邈醢醣鍪镡镢镣镤镥镦镧镨镩镪镫隳霜霞鞠馘骤髀髁魍魏鲼鲽鳃鳄鳅鳆鳇鳊鳋鹩鹪鹫鹬麋黏黛黜黻鼢鼾龋龌龠黝瞭簕',
    18: '冁嚣彝懵戳曛曜檫瀑燹璧癔癖癜癞瞻瞽瞿礓礞簟簦簪糨翻艟藕藜藤藩蟛蟠蟪蟮襟覆謦蹙蹦蹩躇邋醪鎏鏊镬镭镯镰镱雠鞣鞫鞭鞯颢餮馥髂髅鬃鬈鳌鳍鳎鳏鳐鹭鹰黟黠鼬鹱鹮蹚瀍藠鞥',
    19: '嚯孽巅攀攉攒曝瀚瀛瀣爆瓣疆癣礤簸簿籀籁缵羸羹艨藻藿蘅蘑蘧蟹蟾蠃蠊蠓蠖襞襦警谶蹬蹭蹯蹰蹲蹴蹶蹼蹿酃醭醮醯鏖镲霪霭靡鞲鞴颤骥髋髌鬏魑鳓鳔鳕鳖鳗鳘鳙麒麓麴黢黼鼗贇韡',
    20: '璺嚷嚼壤孀巍攘曦瀵瀹灌獾瓒矍籍糯纂耀蘖蘩蠕蠛譬躁躅酆醴醵镳霰颥馨骧鬓魔鳜鳝鳞鳟黥黧黩黪鼍鼯孃鱀龑黁',
    21: '夔曩灏爝癫礴禳羼蠡蠢赣躏醺鐾露霸霹颦髓鳢麝黯鼙罍',
    22: '囊懿氍瓤穰耱蘸蘼躐躔镶霾饔饕髑鬻鹳龢',
    23: '麟攥攫癯罐趱躜颧鬟鼷鼹齄蠲',
    24: '灞矗蠹衢襻躞鑫囍',
    25: '鬣馕囔戆攮纛',
    26: '蠼',
    27: '',
    28: '',
    29: '',
    30: '爨',
    31: '',
    32: '',
    33: '',
    34: '',
    35: '',
    36: '齉',
};

const zhStrokeList = {
    0: '',
    1: '',
    2: '',
    3: '三',
    4: '戶內勻弔',
    5: '冊処氹囘氾',
    6: '丟扡汙兇伕亙攷阬阯迆吒',
    7: '貝別車沈沖囪兌夾見決呂沒刪禿吳災妝壯佔佈佇沍吶囯佘',
    8: '並長東兒岡矽糾屆況來兩侖門妳臥協亞軋爭狀廼於昇戔郃彿咼牀拋羋虯苧姍歿卹玨秈靣',
    9: '訂飛風負訃軌紅後級計紀勁荊軍侶卻紉陝屍帥韋俠洶彥頁約則柵貞陣茲迴為厛閂衹厙姦紆紂紇紈釓釔貟祐剋剄奐侷係陘姪兗剎巹牴枴盃洩郤郟俁坰恆凃祕垵炤耑',
    10: '剝狽畢財倉陳恥純島釘凍鬥紡紛剛個宮貢剮華記莢莖徑晉庫倆連陸倫馬脈們畝納紐豈氣訖殺紗閃師時書孫討條紋烏務峽狹挾脅軒訓訊陰郵娛員悅這針紙莊紥芻訌訕訐飢倣倀託唚唄垻峴紕紜紓莧軔釗釕釙珮哢倖弳涇眎玆脩浹毧唕皰涖旂',
    11: '敗絆閉貶缽參側産處從帶釣頂動隊訛釩販訪婦崗夠貫規國過貨堅將階淨進訣萊淚涼淩婁鹵掄淪麥覓鳥貧淒啓牽釺淺強氫頃區軟掃紹設紳視術訟貪屜烴脫偉問渦習細現鄉許敘啞訝陽異魚責紮斬張帳偵掙猙執終晝專著組乾產眾偽釦捫硃詎訥閆圇娬悵桿萇厠捲匭週崠絃唸崐梘訢梟紲紺紱紼絀紿捨軛埰鄆釷釤釵釧敍偺埡堊婭崢掛梔脛隉悽脣堝媧崍徠梱淶萵袞偸勗啗菴淥釬畧毬眥訦隄琍彫',
    12: '綁報備筆補殘廁測場鈔創詞湊達貸單盜棟鈍惡貳發琺飯費馮鈣稈臯給鈎貴賀壺畫換喚渙黃揮葷渾禍極幾間堿揀減絞痙廄傑結絕鈞開凱殼塊勞祿絡買貿悶鈉惱甯鈕評棲棄喬欽韌絨閏傘喪腎勝稅順絲訴筍湯貼統萬違圍爲葦無閑廂項虛須絢尋硯揚堯爺葉壹飲喲詠湧猶遊馭淵雲鄖隕運棗詐棧脹診幀衆軸貯鄒詛裡詆厤鉅傖喫嵗粬鄔堦躰睏媯嵐幃溈詁証訶詘詔詒閔閎閌靭飪飩飭飫剴喦棖註黽勛堖復惻惲湞硨絝絎絳腖揹葒葯葤覘貰賁貺貽軻軲軤軹軼軫軺鈈鈑鈦鈐鈁鈥鈧鈄鈀頇傢傚棬椏氬畱絛雋愜瑯硤牋腡啣崳蒐湣硶琯痠',
    13: '愛奧頒飽鉑蒼滄詫腸誠馳傳誕當搗滌遞電叠頓煩楓該蓋幹溝詭號嘩話煥毀賄會彙際賈鉀腳較節經絹僅誇裏蓮漣煉賃鈴虜賂亂媽嗎鉚夢滅腦農鉛鉗嗆搶傾傷聖獅詩勢飾試飼頌肅綏歲損塗蛻馱頑溫窩嗚塢廈羨詳詢馴遜煙楊搖遙業義詣蔭傭鈾與預園圓遠粵暈載賊閘債盞睜腫誅裝資匯傴鳧獁僉鉋徬愾愴煬暘鳩搆遝煒瑋骯塋煢誆誄詿詼詰詵詮詬諍詡牐閙飴禕嗶塏愷瑉猻竪蓽蓀廕跡嫋塚準嗩塤塒蓆暉慄楨琿禎綆筧綃綉綈蓧蒔蒓蜆貲賅賍輊軾輇輅鈺鈷鉦鈸鈳鉆鈽鉬鉭鉄鉕鈿鉞鈰鉈鉉鉍鈹鈮頊頎頏傯僂嗇摑瘂覜勣羥剷媼慍搇稜溼睞蛺煇耡鉲勦尲煖煆痺稟酧鄘搾',
    14: '幣賓餅駁蔔慚慘摻嘗暢塵稱綢綽蔥鄲鄧墊對奪墮爾餌罰閥瘋鳳輔複趕綱閣鉻構慣廣閨滾漢閡滬劃瘓誨夥監箋漸蔣鉸僥餃稭誡緊盡摳寬厲鄰領摟鋁屢綠綸瑪滿麽綿閩鳴銘瘧嘔漚頗齊塹槍僑寢輕認榮賒滲蝕實適壽說碩誦隨瑣態銅圖團窪網維僞聞撾蝸誣誤銑銜緒厭瘍養瑤銥銀熒誘漁語獄劄嶄綻漲趙鄭滯種墜綴漬綜寧塼厰麼僕勱嘆関薌滷獃嫗匲嶇誌慪摺摶槓榪蓯製凴徴戧熗碭綫膁蔦鳶慟暱滸碸滎犖誚誑誥誒颮颯餉滻餄餎嘜慳輓榿慇樺燁貍僨匱嘖幗綵幘愨摜殞璉皸槃緋緔綾綺綬綹緄綣綳綰綞緇蔆覡賕賑輒銬銪鋮鉺銱鋣銠銦鋌銓銖銩鉿錚銚銫銃銣銨蔴僳嘍嶁巰摣箠槨漵箏緍蔞僱慴瘉氳膃戩鞀髩',
    15: '皚罷輩鋇編標撥層廠徹撐遲齒廚鋤瘡賜撣憚彈蕩導敵締調賭緞餓範誹廢墳憤鋒膚撫賦鞏劊輥緩輝緝價駕緘儉踐賤劍澗漿槳獎膠澆嬌駒劇潔課褲儈潰撈澇樂憐練輛諒遼凜劉樓魯慮輪論碼罵賣邁貓冪緬廟憫撓鬧餒撚諾歐毆盤賠噴潑撲鋪遷潛請慶窮確熱銳潤賞審駛樞豎數誰慫撻談歎銻駝橢緯衛蕪蝦賢險線銷寫鋅噓選鴉樣窯遺儀億誼瑩憂緣閱暫賬摯幟質皺豬諸駐樁諄銼嘰戯儅餘嘸墰嶴廡憮蕓穀鄴敺儂劌颳噝樅氂甌蝨槼鄶駔駙駘駟噅噠嘵嬈嶠縂撟殤潯衚蕘蕎蕁蕒鴆鴇頫嘮嫺嶗噁曄潿蕕諏諛諑諉諗諂誶閫閬餑嬋慙慼樑慾澠墝賚鋏麩璡憒槧毿蝟緙緗緹緲緱緶緦縋潟蕆醃蕢褳賫賡輦賧輞輟輜舖鋱鋥鋰鋯銹鋨鋶錒鋟鋃鋦靚頡頦頜潁魷魴翬擕潷辤廝瘞篋踡鋂澂鋜',
    16: '辦鮑憊艙熾醜錘錯擔擋燈澱諜錠獨噸奮縫諷輻縛鋼館龜鍋駭橫還謊諱獲機積輯薊劑頰撿薦鍵餞頸靜舉據鋸錦墾窺賴勵曆龍盧擄錄駱螞瞞錨錳謎謀膩濃頻憑樸錢薔橋親薩篩燒輸樹壇燙縧頭頹鴕謂錫鍁縣餡憲蕭曉嘯諧興學勳鴨閹閻諺鴦頤彜憶隱螢營穎擁踴嶼鴛擇澤戰鍺築磚錐濁鄺儘璣儕嶧懌曇霑燉麅縐餚錶諢鍆隷儔鬨噦噲噥薑澮獪瞘篤薈螘駢橈獫緻薟賮鴣鴝鴟鴞燜縈謔謁諫諼諤諳諦諮諞諭醖閾閿閶閽閼舘餛諝餜儐殫燄禪篳螄諡錸黿縉嬡噯磧穌篠縟縝縞縭縊縑靦懞簑蕷覦輳鍩錁錛鋻錆鍀錕鍃錮錇錈錟錙頷鮁鮒鮃鮎鮐墻嬙糢橰瘺窶镟篛錼骾鮌廩懍燐',
    17: '襖幫謗繃斃濱擯燦償騁儲聰膽檔點鍍鍛糞擱鴿購韓鴻環燴擊績擠濟艱檢講矯舊駿顆懇虧擴闊藍闌濫禮隸聯斂臉療臨嶺簍縷鎂彌擬獰擰濘膿謙牆鍬趨賽澀聲濕聳雖縮擡濤謄濰甕戲轄嚇鮮謝壓謠嬰應優輿禦轅嶽醞鍘齋氈輾蟄擲鍾謅燭賺贅總縱嬭嚮壙獷襍縴餳歟霛磯闈璵嚀闆癘邇擧嚌檉櫛澩薺盪藎濜幬檜濬膾覬齔殮凟蹌鴰鴯鵂礄褻賸餵嶸匵癆癇縶襇謨謖謐闃濶餷闋颶餿餽嬪轂谿癉蹕曖縯璦簀糝縹縵縲繆繅蟈覯賻鍥鍇鍔鍤鍫鍶鎪鍰鎄鮫鮚鮭鮞鮪襆屨檣餱耬螻蟎魎檁嬤繈臒',
    18: '翺擺鎊邊蟬蟲雛礎闖叢竄禱斷鵝額豐鎬歸櫃穢雞繭簡檻濺醬鵑謹燼曠壘離鯉鏈糧獵餾隴濾謾謬攆聶鎳檸臍騎翹竅瓊軀擾繞繕嬸繩雙擻鎖題鎢霧瀉顔藥醫藝蠅雜鎮織職轉蹤鎸鼕檯謳毉癤餼鬆濼檾蟣釐懟懕瀏禰鞦蠆闓贄騐嚙瀋燾襠鎧鏵鎩鬩騏騍騅櫂瞼繢蟯襝觴蹠鵓鵒鵜鵠鵐攄霤瀅耮鎣謫闔闐闕韙韞饃颼饈檳殯瀦燻簞甖臏鯗嚕擼擷璿簣繒繚聵覲覰賾鏌鎘轆鎦鎰鎵顓顏顎鯇鯊鯽鎔穡藪懣蹣繙燿',
    19: '藹礙癟瀕鏟懲癡寵疇櫥辭顛犢關懷壞繪譏繳轎鯨鏡礦臘懶類麗瀝簾嚨壟攏蘆廬羅饅難龐鵬騙蘋譜簽勸鵲騷識獸爍蘇獺譚襪穩蠍繡嚴癢蟻繹願蘊韻贊贈轍證璽聼嚦壢藶隴壚瀘嚥櫟艤覈礪躉鶇厴鏇蟶牘鏗騗騖騭曡蹺醯穨鵪鵯鵡鶓鶉躂櫧瀠瀟簫羆藺譖譙譎鏘闞饉韜鏤譆櫫蘄襤觶嬾瀨氌櫞櫓繾繰繮繯轔鏨贋鏢鏜鏍鏑鏞鏝顙鏃鯖鯤鯫鯢鯛鯪鯧鯝鯡鯔鯴鯰贇簷羶騣韝',
    20: '寶辮攙闡襯籌觸黨礬護饑繼艦競覺饋攔籃蘭瀾礫鐐齡爐飄譴饒鰓贍釋騰犧鹹獻響懸議譯贏譽竈齣懺懽纊嚨壟寵龐櫪瀧蘢騶櫨臚鐘闥驊孃癥蠔獼糲聹蠣鐃鐋嚳蠐鐒鐧饗櫬櫸竇繽驀騮驁騫騸齙齟齠嚶攖瀲蘞衊鐨鶘鶚鶿鶻鶥鶩蠑譫饌飃饊譟顢鹺鐔蘚鐝鐓鏷鐠鐙鏹鰌鰈鰒鯿鰐鰉鰠髏',
    21: '辯纏躊顧鶴轟歡殲懼巋蠟欄覽爛鐳鐮騾齧驅權攝懾屬鐵囂攜鏽續櫻躍贓髒囈灃攏儸襯儷儼櫳瓏癧朧酈臟鶯鐸囀櫺纍鐺驂儺囁灄灕蘺闢躋驃驄齜齦攛瓔纈躑韃鞽鰱鰷鰣鷂鶼斕癮飈飆嚻癩籐鑊鐿顥鰲鰭鰩鰥鰨儹',
    22: '顫疊讀龔鑒驕驚聾籠巒孿蘿邏鷗竊灑贖攤灘體聽彎襲癬攢鑄囌糴孌轢驍礱邐玀艫鷙囉儻覿躒譾躚轡籜霽躓鑌驏鰹齪齬癭鷚鷓囅鑔躕籟鰻鰵鱈鱉鰳鰾鱅鼴',
    23: '變鼈蠱攪戀鱗攣黴曬纖顯驗纓癰驛轤籥欒鑠鷥龕籠聾襲龔欏鷳鱘靨饜魘齏讌鑥鱝鷦鷯鷲鷸讎巔髖髕鑣瓚顬鱖鱓鱒黲',
    24: '壩蠶讒贛觀鹼讕攬籬靈釀齲讓癱鹽鷹驟囑韆艷籩靂齶鱟黌籪羈齷鷺鸌讖靄灝癲顰鼉鱧',
    25: '饞躥顱籮蠻廳灣鑲鑰糶臠欖鱭躡鑭韉纘齇',
    26: '驢灤鑷顴釁矚讚鬮釅釃鑹驥躪趲躦',
    27: '纜鑼鑽鸕讜灧鱸鑾讞顳驤黷',
    28: '豔鑿鸚鸛戇',
    29: '鬱驪',
    30: '鸞鸝鱺饢',
    31: '',
    32: '籲',
    33: '',
    34: '',
    35: '',
    36: '龜'
};

const getStroke = c => {
    // 不符合的就擺到最後
    let res = DefaultStroke;

    // 比對繁體
    Object.keys(zhStrokeList).forEach(num => {
        if (zhStrokeList[num].indexOf(c) > -1) {
            res = num;
        }
    });

    // 比對簡體
    if (res === DefaultStroke) {
        Object.keys(cnStrokeList).forEach(num => {
            if (cnStrokeList[num].indexOf(c) > -1) {
                res = num;
            }
        });
    }

    return res;
};

export { getStroke };
